deutex (5.2.3-1) unstable; urgency=medium

  [ Alexandre Detiste ]
  * replace transitional pkg-config by new pkgconf

  [ Fabian Greffrath ]
  * New upstream version 5.2.3
  * Bump Standards-Version to 4.7.0
  * Update my Debian packaging copyright years

 -- Fabian Greffrath <fabian@debian.org>  Fri, 21 Feb 2025 14:25:46 +0100

deutex (5.2.2-1) unstable; urgency=medium

  * New upstream version 5.2.2
  * Remove patches, applied upstream.
  * Bump debhelper-compat to 13.
  * Bump Standards-Version to 4.5.1.
  * Rules-Requires-Root: no.
  * Remove "-Wl,--as-needed" linker flag.
  * Bump debian/watch version to 4.
  * Run "wrap-and-sort -asb".
  * Add some real packages to alternative dependencies.

 -- Fabian Greffrath <fabian@debian.org>  Sat, 02 Jan 2021 21:51:16 +0100

deutex (5.2.1-2) unstable; urgency=medium

  * Remove myself from Uploaders.

 -- Jonathan Dowland <jmtd@debian.org>  Fri, 08 May 2020 15:06:19 +0100

deutex (5.2.1-1) unstable; urgency=medium

  * New upstream version 5.2.1
  * Add a post-release commit from upstream to check for manpage
    generation support by default.
  * Bump debhelper-compat to 12.
  * Bump Standards-Version to 4.4.0.

 -- Fabian Greffrath <fabian@debian.org>  Tue, 03 Sep 2019 12:27:06 +0200

deutex (5.2.0-1) unstable; urgency=medium

  * New upstream version 5.2.0

 -- Fabian Greffrath <fabian@debian.org>  Mon, 08 Jul 2019 11:21:43 +0200

deutex (5.1.2-1) unstable; urgency=medium

  * New upstream version 5.1.2
  * Bump Standards-Version to 4.2.0

 -- Fabian Greffrath <fabian@debian.org>  Tue, 21 Aug 2018 16:01:27 +0200

deutex (5.1.1-1) unstable; urgency=medium

  * New upstream version 5.1.1.
  * Update Vcs-* to point to salsa.debian.org.
  * Remove patches applied upstream.
  * Bump debhelper compat to 11.
  * Bump Standards-Version to 4.1.3.
  * Use secure copyright format URI.

 -- Fabian Greffrath <fabian@debian.org>  Sun, 28 Jan 2018 22:01:18 +0100

deutex (5.1.0-3) unstable; urgency=medium

  * Point Homepage field to the GitHub project page
  * Fix indentation in debian/rules.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 18 Dec 2017 21:41:03 +0100

deutex (5.1.0-2) unstable; urgency=medium

  * Install NEWS.adoc as upstream changelog.
  * Add patch from upstream GIT to increase the
    array size for a char variable.

 -- Fabian Greffrath <fabian@debian.org>  Mon, 18 Dec 2017 21:19:58 +0100

deutex (5.1.0-1) unstable; urgency=medium

  * New upstream version 5.1.0.
  * Add myself to Uploaders.
  * Bump debhelper compat to 10.
  * Convert debian/copyright into machine-readable format.
  * Bump Standards-Version to 4.1.2.
  * Fix vcs-field-uses-insecure-uri lintian warnings.
  * Enable all hardening flags.
  * Add pkg-config to Build-Depends.

 -- Fabian Greffrath <fabian@debian.org>  Fri, 15 Dec 2017 20:54:45 +0100

deutex (5.0.0-1) unstable; urgency=medium

  * New upstream version.
  * Build-depend on docbook-xml and friends.
    Thanks Adrian Bunk. Closes: #870046.

 -- Jonathan Dowland <jmtd@debian.org>  Mon, 31 Jul 2017 22:53:54 +0100

deutex (5.0.0~beta.2-2) unstable; urgency=medium

  * Update Vcs-Git to the anonscm URL scheme
  * explicit version depends on automake to avoid a spurious problem
    regenerating the autocruft
  * Add asciidoc-base to Build-Depends so we build the manpage.
  * Run wrap-and-sort over the control file.
  * Previous upload Closes: #571992, #610679.

 -- Jonathan Dowland <jmtd@debian.org>  Thu, 27 Jul 2017 21:54:53 +0100

deutex (5.0.0~beta.2-1) unstable; urgency=medium

  * New upstream version (!)
    * deusf binary no longer supplied
    * old docs files removed
    * now supports PNGs
  * Major packaging overhaul:
    * move to dh 9 and use dh auto rules in Makefile.
    * All patches merged upstream so delete debian/patches
    * Use source format 3.0 (Quilt), no need for explicit quilt dependency
    * bump standards version.
    * partially fix watch file.
    * Update my full name in Uploaders: header.

 -- Jonathan Dowland <jmtd@debian.org>  Wed, 26 Jul 2017 11:39:26 +0100

deutex (4.4.902-13) unstable; urgency=low

  * Recommend either boom-wad or doom-wad, since the latter is
    not satisfyable in main. Closes: #682132.

 -- Jon Dowland <jmtd@debian.org>  Fri, 20 Jul 2012 17:08:20 +0100

deutex (4.4.902-12) unstable; urgency=low

  * Update control file to reflect new VCS location
  * add misc:Depends to quieten lintian
  * bump standards version
  * add a README.source

 -- Jon Dowland <jmtd@debian.org>  Fri, 03 Sep 2010 16:03:44 +0100

deutex (4.4.902-11) unstable; urgency=low

  [ Cyril Brulebois ]
  * Added XS-Vcs-Svn and XS-Vcs-Browser fields in the control file.

  [ Barry deFreese ]
  * Bump debhelper build-dep to match compat
  * Make clean not ignore errors
  * Remove XS- from VCS fields
  * Upgrade watch file to version 3

  [ Ansgar Burchardt ]
  * debian/control: Add Homepage field

  [ Jon Dowland ]
  * change menu area to Games/Tools.
  * use quilt for patch management.
  * bump standards version to 3.8.2.
  * add a lintian-check passing copyright line to debian/copyright
  * Update my e-mail address in Uploaders:
  * debhelper up to 7.

 -- Jon Dowland <jmtd@debian.org>  Thu, 09 Jul 2009 18:53:06 +0100

deutex (4.4.902-10) unstable; urgency=low

  * update standards version to 3.7.2
  * change Maintainer to Debian Games Team
  * remove dh_link
  * add doom-wad to Recommends: (most features require an IWAD)
  * fix path to deutex in menu entry (thanks ads)
  * add dh_installmenu
  * remove redundant dh_installman

 -- Jon Dowland <jon@alcopop.org>  Tue, 12 Sep 2006 23:51:24 +0100

deutex (4.4.902-9) unstable; urgency=low

  * remove stray files (tags,op,op2,gcc4-)
  * don't bother fixing ./docsrc/deutex.6 (since we don't use it)
  * check for not-logging before writing to the log FP to avoid
    segfaults when CWD is a read-only filesystem: see
    <http://www.freelists.org/archives/yadex/03-2006/msg00002.html>

 -- Jon Dowland <jon@alcopop.org>  Thu,  6 Apr 2006 16:55:26 +0100

deutex (4.4.902-8) unstable; urgency=low

  * do not invoke dh_installexamples
  * updated policy
  * install the FAQ

 -- Jon Dowland <jon@alcopop.org>  Tue, 14 Mar 2006 22:14:25 +0000

deutex (4.4.902-7) unstable; urgency=low

  * look for the IWADs in /usr/share/games/doom in addition to $CWD
  * updated maintainer field

 -- Jon Dowland <jon@alcopop.org>  Sun,  5 Mar 2006 16:45:31 +0000

deutex (4.4.902-6) unstable; urgency=low

  * add support DEB_BUILD_OPTIONS containing "debug"

 -- Jon Dowland <jon@dowland.name>  Sat, 12 Nov 2005 17:01:31 +0000

deutex (4.4.902-5) unstable; urgency=low

  * add /usr/games to dirs

 -- Jon Dowland <jon@dowland.name>  Sun, 16 Oct 2005 13:15:37 +0100

deutex (4.4.902-4) unstable; urgency=low

  * remove unixtmp1 stuff that got stranded from a build
  * yet more (c) -> \(co substitution in manpage

 -- Jon Dowland <jon@dowland.name>  Sat, 15 Oct 2005 18:00:41 +0100

deutex (4.4.902-3) unstable; urgency=low

  * add the lzw.c licence notes
  * remove groff-ism in the manpages

 -- Jon Dowland <jon@dowland.name>  Wed, 12 Oct 2005 18:36:15 +0100

deutex (4.4.902-2) unstable; urgency=low

  * put binaries in /usr/games
  * change acute-accent in upstream maintainer's name for roff
  * clarify copyright holders
  * remove misc:depends from control (not used)

 -- Jon Dowland <jon@dowland.name>  Sun,  9 Oct 2005 22:04:52 +0100

deutex (4.4.902-1) unstable; urgency=low

  * New upstream release

 -- Jon Dowland <jon@dowland.name>  Mon, 26 Sep 2005 22:34:12 +0100

deutex (4.4.0-3) unstable; urgency=low

  * Remove some dpatch relics
  * Clean-up rules file
  * Fulfilling ITP. Closes: #322473.

 -- Jon Dowland <jon@dowland.name>  Thu, 1 Sep 2005 23:40:35 +0100

deutex (4.4.0-2) unstable; urgency=low

  * prepared package for upload. Closes: #322473.

 -- Jon Dowland <jon@dowland.name>  Wed, 17 Aug 2005 22:48:01 +0100

deutex (4.4.0-1) unstable; urgency=low

  * Initial Release.

 -- Jon Dowland <jon@dowland.name>  Wed, 10 Aug 2005 21:09:59 +0100
